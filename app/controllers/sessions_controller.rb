class SessionsController < ApplicationController

def new
end

def create
  auth = request.env["omniauth.auth"]
  if auth
    if(User.find_by_provider_and_uid(auth['provider'], auth['uid']))
      user = User.find_by_provider_and_uid(auth['provider'], auth['uid'])
      user.views = user.views + 1
      user.save
    else
      user = User.create_with_omniauth(auth)
    end
  else
    user = User.authenticate(params[:email], params[:password])
  end

    if user
      session[:user_id] = user.id
      redirect_to root_url, :notice => "Logged in!"
    else
      flash.now.alert = "Invalid email or password"
      render "new"
    end

end

def destroy
  session[:user_id] = nil
  redirect_to root_url, :notice => "Logged out!"
end

end
