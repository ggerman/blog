class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]

  def home
    @articles = Article.all.page params[:page]
  end

  def new
    @user = current_user
  end

  def create
    @article = Article.new(article_params)

    respond_to do |format|

      if @article.save
        format.html  { redirect_to(@article,
                      :notice => 'Post was successfully created.') }

        format.json  { render :json => @article,
                      :status => :created, :location => @article }
      else
        format.html  { redirect_to(@article,
                       :notice => @article.errors.messages.values.each { |attr, message| }
                                  ) }

        format.json  { render :json => @article.errors,
                      :status => :unprocessable_entity }
      end
    end
  
  end

  def index
    @articles = Article.where(users_id: current_user[:id]).page params[:page]
    respond_to do |format|
      format.html 
      format.json
    end
  end

 # GET /articles/1/edit
  def edit
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @article = Article.find(params[:id])
    @user = User.find(@article.users_id)

    if current_user
      @rating = Rating.where(article_id: @article.id, user_id: current_user.id).first
      unless @rating 
      @rating = Rating.create(article_id: @article.id, user_id: current_user.id, score: 0)
      end
    end

  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
 
    respond_to do |format|
      format.html { redirect_to(@article, 
                    :notice => 'Post was successfully removed.') }
      format.json { head :no_content }
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :text, :images, :users_id)
  end 

end
