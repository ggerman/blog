class UsersController < ApplicationController

 def index
 end

 def new
   @user = User.new
 end

 def show
   @user = current_user
   if @user.profile.nil?
     @profile = current_user.build_profile
   else
     @profile = @user.profile

   end
 end

 def create 
   @user = User.new(params[:user])
   if @user.save
     redirect_to root_url, :notice => "Signed up!"
   else
     render "new"
   end
 end

end
