class Profile < ActiveRecord::Base
  attr_accessible :phone, 
                  :avatar, 
                  :mobile_phone, 
                  :address, 
                  :status, 
                  :city, 
                  :country, 
                  :company, :blog, :extras, :user_id


  mount_uploader :avatar, AvatarUploader

  belongs_to :user
end
