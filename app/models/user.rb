class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  attr_accessible :email, :password, :password_confirmation
  attr_accessor :password

  before_save :encrypt_password

  has_one :profile
  has_many :ratings
  has_many :articles, dependent: :delete_all, validate: :false

  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_confirmation_of :email
  validates_uniqueness_of :email

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def self.create_with_omniauth(auth)
    create! do |user|
      user.uid = auth["uid"]
      user.provider = auth["provider"]
      user.email =  auth["info"]["email"] ? auth["info"]["email"] : auth["uid"]
      user.name = auth["info"]["name"]
      user.password = auth["provider"]
      user.views = 0
    end
  end

  def encrypt_password 
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
end
