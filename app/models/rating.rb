class Rating < ActiveRecord::Base
  attr_accessible :article_id, :user_id, :score

  belongs_to :article
  belongs_to :user
end
