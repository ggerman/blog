class ChangeFieldToProfiles < ActiveRecord::Migration
  def change
    def up
      remove_column :accounts, :user_id
      add_reference :profiles, :user, index: true
    end

    def self.down
      remove_column :profiles, :user_id
      add_column :profiles, :user_id, :integer
    end
  end
end
