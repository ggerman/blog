class AddImagesToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :images, :text
  end

  def self.down
    remove_column :articles, :images
  end
end
