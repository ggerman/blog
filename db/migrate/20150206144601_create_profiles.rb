class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :phone
      t.string :mobile_phone
      t.string :avatar
      t.string :address
      t.string :status
      t.string :city
      t.string :country
      t.string :company
      t.string :blog
      t.text :extras

      t.timestamps
    end
  end

  def self.down
    drop_table  :profiles
  end
end
